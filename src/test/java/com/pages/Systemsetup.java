package com.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.scripts.Keywords;
public class Systemsetup {/*
	// **COST AND PRICING**
	final WebDriver driver;
	public static Keywords keywords;
	// Display On Inventory Screen
	public static final By standardcost = By.id("cbInvStdCost");
	public static final By Averagecost = By.id("cbInvAvgCost");
	public static final By Lastcost = By.id("cbInvLstCost");

	// Calculate Price By
	public static final By Margin = By.id("rbCalcPrcByMargin");
	public static final By Markup = By.id("rbCalcPrcByMarkup");
	public static final By DefaultPrecentage = By.id("txtDefaultMargin");

	// Calculate Daily Sales Gross Margin From
	public static final By Last = By.id("rbCalGrossMarginLast");
	public static final By Average = By.id("rbCalGrossMarginAvg");
	public static final By Standard = By.id("rbCalGrossMarginStd");

	// Gift Cards
	public static final By GiftCardMask = By.id("txtGiftCardMast");
	public static final By Changebackat = By.id("txtChangeBack");
	public static final By Giftcardmin = By.id("txtGiftCardMin");

	public static final By AllowReloadofgiftcard = By.id("cbReloadGiftCard");

	public Systemsetup(WebDriver driver) throws IOException {
		this.driver = driver;
		Keywords keywords=Keywords.getInstance();
	}

	public static void systmsetupCost() {

		*//** Standard cost **//*
		keywords.isElementPresent(standardcost);
		keywords.click(standardcost);	
		*//** Average cost **//*
		if (keywords.isElementPresent(Averagecost)) {
			keywords.click(Averagecost);

		}

		*//** Last cost **//*

		if (keywords.isElementPresent(Lastcost)) {
			keywords.click(Lastcost);

		}

		*//** Margin **//*

		if (keywords.isElementPresent(Margin)) {
			driver.findElement(Systemsetup.Margin).click();

		}

		*//** Mark up **//*

		if (keywords.isElementPresent(Systemsetup.Markup).isDisplayed()) {
			driver.findElement(Systemsetup.Markup).click();

		}

		*//** Default percentage **//*

		if (keywords.isElementPresent(Systemsetup.DefaultPrecentage)
				.isDisplayed()) {
			driver.findElement(Systemsetup.DefaultPrecentage).sendKeys(
					"50");

		}

		*//** Gift card Mask **//*

		if (keywords.isElementPresent(Systemsetup.GiftCardMask).isDisplayed()) {
			driver.findElement(Systemsetup.GiftCardMask).sendKeys(
					"9?????????");
		}

		*//** Change Back at **//*

		if (keywords.isElementPresent(Systemsetup.Changebackat).isDisplayed()) {
			driver.findElement(Systemsetup.Changebackat).sendKeys("2");
		}

		*//** Gift card minimum **//*

		if (keywords.isElementPresent(Systemsetup.Giftcardmin).isDisplayed()) {
		}

		
		 * if(driver.findElement(Systemsetup.AllowReloadofgiftcard).
		 * isDisplayed()){
		 * driver.findElement(Systemsetup.AllowReloadofgiftcard
		 * ).click();
		 * System.out.println("Clicked on allow reload gift card check box");
		 * }else{
		 * System.out.println("Allow reload gift card check box not found on page "
		 * ); }
		 

		*//** Retain cost change price **//*

		if (keywords.isElementPresent(Systemsetup.RetainoncostchangePRICE)
				.isDisplayed()) {
			driver.findElement(Systemsetup.RetainoncostchangePRICE)
					.click();
		}

		*//** Retain cost change Margin mark up **//*

		if (keywords.isElementPresent(
				Systemsetup.RetainoncostchangeMARGINMARKUP).isDisplayed()) {
			driver.findElement(
					Systemsetup.RetainoncostchangeMARGINMARKUP).click();
		}

		*//** Add on Field1 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField1).isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField1).sendKeys(
					"User Defined 1");
		}

		*//** Add on Field1 tax N **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField1taxN)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField1taxN).click();
		}

		*//** Add on Field1 tax 1 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField1tax1)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField1tax1).click();
		}

		*//** Add on Field1 tax 2 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField1tax2)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField1tax2).click();
		}

		*//** Add on Field1 tax 3 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField1tax3)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField1tax3).click();
		}

		*//** Add on Field1 tax 4 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField1tax4)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField1tax4).click();
		}

		*//** Add on Field2 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField2).isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField2).sendKeys(
					"User Defined 2");
		}

		*//** Add on Field2 tax N **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField2taxN)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField2taxN).click();
		}

		*//** Add on Field2 tax 1 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField2tax1)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField2tax1).click();
		}

		*//** Add on Field2 tax 2 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField2tax2)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField2tax2).click();
		}

		*//** Add on Field2 tax 3 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField2tax3)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField2tax3).click();
		}

		*//** Add on Field2 tax 4 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField2tax4)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField2tax4).click();
		}

		*//** Add on Field3 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField3).isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField3).sendKeys(
					"User Defined 3");
		}

		*//** Add on Field3 tax N **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField3taxN)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField3taxN).click();
		}

		*//** Add on Field3 tax 1 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField3tax1)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField3tax1).click();
		}

		*//** Add on Field3 tax 2 **//*

		if (keywords.isElementPresent(Systemsetup.ADDONField3tax2)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField3tax2).click();
		}

		*//** Add on Field3 tax 3 **//*

		if (driver.findElement(Systemsetup.ADDONField3tax3)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField3tax3).click();
		}

		*//** Add on Field3 tax 4 **//*

		if (driver.findElement(Systemsetup.ADDONField3tax4)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ADDONField3tax4).click();
		}

	}

	// Operation Defaults
	public static void systmsetupOpertionDefults(By element, String string) {

		*//** Operation Default tab **//*

		if (driver.findElement(Systemsetup.operation).isDisplayed()) {
			driver.findElement(Systemsetup.operation).click();

		}

		*//** PLU Next **//*

		if (driver.findElement(Systemsetup.pluNxt).isDisplayed()) {
			driver.findElement(Systemsetup.pluNxt).sendKeys("8149");

		}

		*//** PLU Increment **//*

		if (driver.findElement(Systemsetup.pluIncrmnt).isDisplayed()) {
			driver.findElement(Systemsetup.pluIncrmnt).sendKeys("1");

		}

		*//** PLU **//*

		if (driver.findElement(Systemsetup.definvPLU).isDisplayed()) {
			driver.findElement(Systemsetup.definvPLU).click();
		}

		*//** Part Number **//*

		if (driver.findElement(Systemsetup.definvPART).isDisplayed()) {
			driver.findElement(Systemsetup.definvPART).click();
		}

		*//** Description **//*

		if (driver.findElement(Systemsetup.definvDES).isDisplayed()) {
			driver.findElement(Systemsetup.definvDES).click();
		}

		*//** Multiple POs per vendor ALLOW **//*

		if (driver.findElement(Systemsetup.MultiplePOspervendorALLOW)
				.isDisplayed()) {
			driver.findElement(Systemsetup.MultiplePOspervendorALLOW)
					.click();
		}

		*//** Multiple POs per vendor ADVIsE **//*

		if (driver.findElement(Systemsetup.MultiplePOspervendorADVISE)
				.isDisplayed()) {
			driver.findElement(Systemsetup.MultiplePOspervendorADVISE)
					.click();
		}

		*//** Customer tab ID **//*

		if (driver.findElement(Systemsetup.DefaultcustmertabID)
				.isDisplayed()) {
			driver.findElement(Systemsetup.DefaultcustmertabID).click();
		}

		*//** Customer tab Name **//*

		if (driver.findElement(Systemsetup.DefaultcustmertabNAME)
				.isDisplayed()) {
			driver.findElement(Systemsetup.DefaultcustmertabNAME)
					.click();
		}

		*//** Sales Quote Days **//*

		if (driver.findElement(Systemsetup.salequotedays).isDisplayed()) {
			driver.findElement(Systemsetup.salequotedays).click();
		}

		*//** Track freight Cost in Receiving **//*

		if (driver.findElement(Systemsetup.TrackFreghtCostsinReceiving)
				.isDisplayed()) {
			driver.findElement(Systemsetup.TrackFreghtCostsinReceiving)
					.click();
		}

		*//** Log Message **//*

		if (driver.findElement(Systemsetup.LogMessage).isDisplayed()) {
			driver.findElement(Systemsetup.LogMessage).click();
		}

		*//** PO Next **//*

		if (driver.findElement(Systemsetup.poNxt).isDisplayed()) {
			driver.findElement(Systemsetup.poNxt).sendKeys("81");
		}

		*//** PO Increment **//*

		if (driver.findElement(Systemsetup.poIncrmnt).isDisplayed()) {
			driver.findElement(Systemsetup.poIncrmnt).sendKeys("1");
		}

		*//** Customer Preferred Price Group NONE **//*

		if (driver.findElement(Systemsetup.CustmerprefredpricegrpNone)
				.isDisplayed()) {
			driver.findElement(Systemsetup.CustmerprefredpricegrpNone)
					.click();
		}

		*//** Customer Preferred Price Group A **//*

		if (driver.findElement(Systemsetup.CustmerprefredpricegrpA)
				.isDisplayed()) {
			driver.findElement(Systemsetup.CustmerprefredpricegrpA)
					.click();
		}

		*//** Customer Preferred Price Group B **//*

		if (driver.findElement(Systemsetup.CustmerprefredpricegrpB)
				.isDisplayed()) {
			driver.findElement(Systemsetup.CustmerprefredpricegrpB)
					.click();
		}

		*//** Customer Preferred Price Group C **//*

		if (driver.findElement(Systemsetup.CustmerprefredpricegrpC)
				.isDisplayed()) {
			driver.findElement(Systemsetup.CustmerprefredpricegrpC)
					.click();
		}

	}

	// Sales and Period

	public static void systmsetupSalesPeriod(By element, String string) {

		*//** Sales and Pricing Tab **//*

		if (driver.findElement(Systemsetup.saleandpricing)
				.isDisplayed()) {
			driver.findElement(Systemsetup.saleandpricing).click();
		}

		*//** Sun day **//*

		
		 * if(driver.findElement(Systemsetup.LastdayoffiscalweekSUN).
		 * isDisplayed()){
		 * driver.findElement(Systemsetup.LastdayoffiscalweekSUN
		 * ).click(); System.out.println("Clicked on sunday Radio button");
		 * }else{ System.out.println("sunday Radio Button not found on page"); }
		 * 
		 * /** Mon Day *
		 

		if (driver.findElement(Systemsetup.LastdayoffiscalweekMON)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastdayoffiscalweekMON)
					.click();
		}

		*//** TuesDay **//*

		if (driver.findElement(Systemsetup.LastdayoffiscalweekTUE)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastdayoffiscalweekTUE)
					.click();
		}

		*//** WednesDay **//*

		if (driver.findElement(Systemsetup.LastdayoffiscalweekWED)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastdayoffiscalweekWED)
					.click();
		}

		*//** ThursDay **//*

		if (driver.findElement(Systemsetup.LastdayoffiscalweekTHU)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastdayoffiscalweekTHU)
					.click();
		}

		*//** FriDay **//*

		if (driver.findElement(Systemsetup.LastdayoffiscalweekFRI)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastdayoffiscalweekFRI)
					.click();
		}

		*//** SaturDay **//*

		if (driver.findElement(Systemsetup.LastdayoffiscalweekSAT)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastdayoffiscalweekSAT)
					.click();
		}

		*//** January **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearJAN)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearJAN)
					.click();
		}

		*//** February **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearFEB)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearFEB)
					.click();
		}

		*//** March **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearMAR)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearMAR)
					.click();
		}

		*//** April **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearAPRIL)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearAPRIL)
					.click();
		}

		*//** May **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearMAY)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearMAY)
					.click();
		}

		*//** June **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearJUNE)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearJUNE)
					.click();
		}

		*//** July **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearJULY)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearJULY)
					.click();
		}

		*//** August **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearAUG)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearAUG)
					.click();
		}

		*//** September **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearSEP)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearSEP)
					.click();
		}

		*//** October **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearOCT)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearOCT)
					.click();
		}

		*//** November **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearNOV)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearNOV)
					.click();
		}

		*//** December **//*

		if (driver.findElement(Systemsetup.LastmonthoffiscalyearDEC)
				.isDisplayed()) {
			driver.findElement(Systemsetup.LastmonthoffiscalyearDEC)
					.click();
		}

	}

	// Inventory Item Options
	public static void systmsetupInvItemOptions(By element, String string) {

		*//** Inventory item option Default tab **//*

		if (driver.findElement(Systemsetup.InvItemOptionDefault)
				.isDisplayed()) {
			driver.findElement(Systemsetup.InvItemOptionDefault)
					.click();
		}

		*//** Maintain Sales History **//*

		if (driver.findElement(
				Systemsetup.ItemdefaultoptionsMaintainSalesHistory)
				.isDisplayed()) {
			driver.findElement(
					Systemsetup.ItemdefaultoptionsMaintainSalesHistory).click();
		}

		*//** Whole Item Only **//*

		if (driver.findElement(
				Systemsetup.ItemdefaultoptionsWholeItemOnly).isDisplayed()) {
			driver.findElement(
					Systemsetup.ItemdefaultoptionsWholeItemOnly).click();
		}

		*//** Discount able **//*

		if (driver.findElement(
				Systemsetup.ItemdefaultoptionsDiscountable).isDisplayed()) {
			driver.findElement(
					Systemsetup.ItemdefaultoptionsDiscountable).click();
		}

		*//** Apply Tax1 **//*

		if (driver.findElement(Systemsetup.ItemdefaulttaxsApplyTax1)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ItemdefaulttaxsApplyTax1)
					.click();
		}

		*//** Apply Tax 2 **//*

		if (driver.findElement(Systemsetup.ItemdefaulttaxsApplyTax2)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ItemdefaulttaxsApplyTax2)
					.click();
		}

		*//** GeneratePriceTags **//*

		if (driver.findElement(
				Systemsetup.ItemPriceShelfoptionsGeneratePriceTags)
				.isDisplayed()) {
			driver.findElement(
					Systemsetup.ItemPriceShelfoptionsGeneratePriceTags).click();
		}

	}

	// Company Information

	public static void systmsetupCompanyInf(By element, String string) {

		*//** Ship to Company Information tab **//*

		if (driver.findElement(Systemsetup.CompanyInformation)
				.isDisplayed()) {
			driver.findElement(Systemsetup.CompanyInformation).click();
		}

		*//** Ship to Company **//*

		if (driver.findElement(Systemsetup.ShiptoCOMPANY).isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoCOMPANY).sendKeys(
					"Arba");
		}

		*//** Ship to Address 1 **//*

		if (driver.findElement(Systemsetup.ShiptoADDRESS1)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoADDRESS1).sendKeys(
					"hitech");
		}

		*//** Ship to Address 2 **//*

		if (driver.findElement(Systemsetup.ShiptoADDRESS2)
				.isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoADDRESS2).sendKeys(
					"srinagar");
		}

		*//** Ship to City **//*

		if (driver.findElement(Systemsetup.ShiptoCITY).isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoCITY).sendKeys(
					"hyderabad");
		}

		*//** Ship to State **//*

		if (driver.findElement(Systemsetup.ShiptoSTATE).isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoSTATE).sendKeys("AP");
		}

		*//** Ship to Zip **//*

		if (driver.findElement(Systemsetup.ShiptoZIP).isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoZIP).sendKeys("34");
		}

		*//** Ship to Phone 1 **//*

		if (driver.findElement(Systemsetup.ShiptoPHONE1).isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoPHONE1).sendKeys(
					"9865132454");
		}

		*//** Ship to Phone 2 **//*

		if (driver.findElement(Systemsetup.ShiptoPHONE2).isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoPHONE2).sendKeys(
					"9565415853");
		}

		*//** Ship to FAX **//*

		if (driver.findElement(Systemsetup.ShiptoFAX).isDisplayed()) {
			driver.findElement(Systemsetup.ShiptoFAX).sendKeys("");
		}

		*//** Ship to Currency **//*

		new Select(driver.findElement(Systemsetup.ShiptoCURRENCY))
				.selectByVisibleText("India");

		*//** Bill to Company **//*

		if (driver.findElement(Systemsetup.BilltoCOMPANY).isDisplayed()) {
			driver.findElement(Systemsetup.BilltoCOMPANY).sendKeys(
					"arbaretail");
		}

		*//** Bill to Address 1 **//*

		if (driver.findElement(Systemsetup.BilltoADDRESS1)
				.isDisplayed()) {
			driver.findElement(Systemsetup.BilltoADDRESS1).sendKeys(
					"madapur");
		}

		*//** Bill to Address 2 **//*

		if (driver.findElement(Systemsetup.BilltoADDRESS2)
				.isDisplayed()) {
			driver.findElement(Systemsetup.BilltoADDRESS2).sendKeys(
					"banjarahills");
		}

		*//** Bill to City **//*

		if (driver.findElement(Systemsetup.BilltoCITY).isDisplayed()) {
			driver.findElement(Systemsetup.BilltoCITY).sendKeys(
					"hyderabad");
		}

		*//** Bill to State **//*

		if (driver.findElement(Systemsetup.BilltoSTATE).isDisplayed()) {
			driver.findElement(Systemsetup.BilltoSTATE).sendKeys("Ap");
		}

		*//** Bill to ZIP **//*

		if (driver.findElement(Systemsetup.BilltoZIP).isDisplayed()) {
			driver.findElement(Systemsetup.BilltoZIP).sendKeys("22");
		}

		*//** Bill to Phone 1 **//*

		if (driver.findElement(Systemsetup.BilltoPHONE1).isDisplayed()) {
			driver.findElement(Systemsetup.BilltoPHONE1).sendKeys(
					"454631325");
		}

		*//** Bill to Phone 2 **//*

		if (driver.findElement(Systemsetup.BilltoPHONE2).isDisplayed()) {
			driver.findElement(Systemsetup.BilltoPHONE2).sendKeys(
					"981245421");
		}

		*//** Bill to FAX **//*

		if (driver.findElement(Systemsetup.BilltoFAX).isDisplayed()) {
			driver.findElement(Systemsetup.BilltoFAX).sendKeys("");
		}

		*//** Default area code **//*

		if (driver.findElement(Systemsetup.BilltoDEFAULTAREACODE)
				.isDisplayed()) {
			driver.findElement(Systemsetup.BilltoDEFAULTAREACODE)
					.sendKeys("");
		}

		*//** Save **//*

		if (driver.findElement(Systemsetup.save).isDisplayed()) {
			driver.findElement(Systemsetup.save).click();
		}

		driver.switchTo().alert().accept();

	}

*/}
