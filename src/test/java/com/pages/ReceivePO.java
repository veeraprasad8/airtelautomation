package com.pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.scripts.Keywords;



public class ReceivePO {
	WebDriver driver=null;	
	public static Keywords keywords;
public static final By SelectReceivePo=By.xpath(".//*[@id='divRecPOContainer']/table/tbody/tr[2]/td[1]");

public static final By SelectReceivePo1=By.xpath(".//*[@id='divRecPOContainer']/table/tbody/tr[1]/td[1]");

public static final By ReceivePo=By.id("btnReceivePO");

public static final By ReceivePOCancel=By.id("btnClose");

public static final By SelectItem=By.xpath(".//*[@id='divRecPOContainer']/table/tbody/tr[2]/td[2]");

public static final By UpdateInventory=By.id("btnUpdateToInv");

public static final By UpdateINVYes=By.id("btnYes");

public static final By UpdateINVNo=By.id("btnNo");

public static final By EditQty=By.id("btnRecEditQty");

public static final By QuantityReceived=By.id("txtRecvPoRecQty");

public static final By Ok=By.id("btnRecvPoOK");

public static final By QtyReceiveCancel=By.id("btnRecvPoCancel");

public static final By SelectDeleteItem=By.xpath(".//*[@id='divRecPOContainer']/table/tbody/tr[4]/td[2]");

public static final By Delete=By.id("btnDelete");

public static final By DeleteYes=By.id("btnYes");

public static final By DeleteNo=By.id("btnNo");

public static final By ReceivePOClose=By.id("btnClose"); 

public static final By BackOrderThisitem=By.id("btnbckOrd");

public static final By CancelRemainingQty=By.id("btnCancelOrder");

public static final By BackOrderItemCancel=By.id("btnClosewindow");

public static final By ReceivingCancel=By.id("btnRecvPoCancel");
public ReceivePO(WebDriver driver) throws IOException {
	this.driver = driver;
	 keywords=Keywords.getInstance();
}
          
}
