package com.pages;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.scripts.Keywords;

public class LoginPage {

	WebDriver driver=null;
	public static Keywords keywords;
	public static final By username = By.id("username");
	public static final By password = By.id("password");	
	public static final By Login = By.xpath(".//*[@id='login-overlay']/div/div[2]/form/button");
	// Utilities
	public static final By Utilities = By.linkText("Utilities");
	// System setup
	public static final By sytemsetup = By.linkText("System Setup");
	// Employee
	public static final By EmployeeMaintenance = By.linkText("Employee Maintenance");
	public static final By Employee = By.linkText("Employee");
	// Inventory
	public static final By Inventory = By.linkText("Inventory");
	// Department
	public static final By Department = By.linkText("Department");
	// Item Maintenance
	public static final By ItemMaintenance = By.linkText("Item Maintenance");
	// Receive PO
	public static final By Receiving = By.linkText("Receiving");
	public static final By ReceivePO = By.linkText("Receive Purchase Order");
	// EditPricetags
	public static final By EditPriceTags = By.linkText("Edit Price Tags");
	// Cash Register Setup
	public static final By CashRegisterSetup = By.linkText("Cash Register Setup");
	public static final By RegisterConfiguration = By.linkText("Register Configuration");
	// Cash Register
	public static final By CashRegister = By.linkText("Cash Register");
	
	public LoginPage(WebDriver driver) throws IOException {
		this.driver = driver;
		 keywords=Keywords.getInstance();
	}
	public void login(){
		keywords.type(username,"");
		keywords.type(password, "");
		keywords.click(Login);
	}
}
