package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.scripts.Keywords;



public class CashRegister {
	final WebDriver driver;
public static final By SelectRegister1=By.xpath(".//*[@id='tblRegisters']/tbody/tr[1]/td[5]/span/a/span");
	
	public static final By SelectRegister2=By.xpath(".//*[@id='tblRegisters']/tbody/tr[2]/td[5]/span/a/span");
	
	public static final By SelectRegister3=By.xpath(".//*[@id='tblRegisters']/tbody/tr[3]/td[5]/span/a/span");
	
	public static final By SelectRegister4=By.xpath(".//*[@id='tblRegisters']/tbody/tr[4]/td[5]/span/a/span");
	
	public static final By SelectRegister5=By.xpath(".//*[@id='tblRegisters']/tbody/tr[5]/td[5]/span/a/span");
	
	public static final By SelectRegister6=By.xpath(".//*[@id='tblRegisters']/tbody/tr[6]/td[5]/span/a/span");
	
	public static final By SelectRegister7=By.xpath(".//*[@id='tblRegisters']/tbody/tr[7]/td[5]/span/a/span");
	
	public static final By SelectRegister8=By.xpath(".//*[@id='tblRegisters']/tbody/tr[8]/td[5]/span/a/span");
	
	public static final By SelectRegister9=By.xpath(".//*[@id='tblRegisters']/tbody/tr[9]/td[5]/span/a/span");
	
	public static final By SelectRegister10=By.xpath(".//*[@id='tblRegisters']/tbody/tr[10]/td[5]/span/a/span");
	
	public static final By Lock0=By.id("regButton2103");
	
	public static final By Lock1=By.id("regButton281");
	
	public static final By Lock2=By.id("regButton283");
	
	public static final By Lock3=By.id("regButton261");
	
	public static final By Lock4=By.id("regButton261 ");
	
	public static final By Lock5=By.id("regButton181");
	
	public static final By Lock6=By.id("regButton181");
	
	public static final By Lock7=By.id("regButton181");
	
	public static final By Lock8=By.id("regButton181");
	
	public static final By Lock9=By.id("regButton181");

	public static final By Employee=By.id("regButton147");
	
	public static final By ManualSalesDepKey=By.id("regButton123");
	
	public static final By SelectManualItem1=By.id("regButton213");
	
	public static final By SelectManualItem2=By.id("regButton215");
	
	public static final By SelectManualItem3=By.id("regButton217");
	
	public static final By SelectManualItem4=By.id("regButton219");
	
	public static final By SelectManualItem5=By.id("regButton237");
	
	public static final By SelectManualItem6=By.id("regButton239");
	
	public static final By SelectManualItem7=By.id("regButton257");
	
	public static final By SelectManualItem8=By.id("regButton259");
	
	public static final By SelectManualItem9=By.id("regButton277");
	
	
	public CashRegister(WebDriver driver,Keywords keyword){ 
		 
	    this.driver = driver; 
	 
	    } 
}
