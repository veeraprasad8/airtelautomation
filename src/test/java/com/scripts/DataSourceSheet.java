package com.scripts;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import org.testng.annotations.Test;





@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface DataSourceSheet {
    String value() default "sheet1";
}


