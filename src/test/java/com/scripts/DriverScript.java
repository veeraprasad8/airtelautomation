package com.scripts;

import java.lang.reflect.Method;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.utilities.AppConfig;
import com.utilities.GenericHelperFunctions;
import com.utilities.InvokeWebDriver;
import com.utilities.Log4jLogger;

public class DriverScript {
	 private static Logger applicationLogs;
	 private static Properties appConfig;
	public  WebDriver driver=null;
	public  Keywords keyword=null;
	public String excelFilePath;
	public static String excelSheetName;
	@BeforeSuite
    public void setUp() {

        try {
            applicationLogs = Log4jLogger.getLog4jInstance();
            applicationLogs.info("Test Suite Execution Started");
            appConfig = AppConfig.getAppConfigInstance();       
            driver=InvokeWebDriver.getWebDriverInstance();
            keyword=Keywords.getInstance();           
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	/*@BeforeClass
	public void dataSheetPaths(String excelPath){
		System.out.println("haiii");
		this.excelFilePath=excelPath;
		//this.excelSheetName=excelSheet;
	}*/
	
	
	@DataProvider(name = "row-based-data")
	public Object[][] createRowBasedData(Method m) {
		applicationLogs.info("trying to create row based data");
		excelFilePath=System.getProperty("user.dir")+appConfig.getProperty("testCaseFilePath");
		Object[][] testData = GenericHelperFunctions.getExcelData(excelFilePath,  findDataSourceSheet(m));
		return testData;
	}
	/**
     * Searches the test method and test class for a {@link com.mckesson.mhs.imports.DataSourceSheet} annotation.
     */
    protected static String findDataSourceSheet(Method m) {       
        if (m.isAnnotationPresent(DataSourceSheet.class)) {
            DataSourceSheet dataSourceSheet = m.getAnnotation(DataSourceSheet.class);   
            System.out.println("value-->"+dataSourceSheet.value());
            return dataSourceSheet.value();
        } else if (m.getDeclaringClass().isAnnotationPresent(DataSourceSheet.class)) {
            DataSourceSheet dataSourceSheet = m.getDeclaringClass().getAnnotation(DataSourceSheet.class);            
            return dataSourceSheet.value();
        }        
        return m.getName(); // Default!
    }
	@AfterSuite
	  public void tearDown() {
		//driver.quit();
	}
}
